��    Z      �     �      �  �   �  �   @     �     �     �     �     �      	  +   	     <	     E	     \	     j	     q	     y	     �	     �	     �	     �	     �	  
   �	     �	     
     5
     M
  	   S
     ]
     }
     �
     �
     �
     �
     �
     �
  !   �
          #     0     >     R     o  	   {  A   �     �     �     �     �     �     �       	             1     D     [     j     z     �  
   �     �     �     �     �     �     �     �               %      ,     M  6   Y     �     �     �     �     �     �     �     �     �  	   �     �     �       <     /   Q  �   �       �  4  �        �     #  
   2     =     P     Y     l  0   u  	   �     �     �     �     �     �                $     <  )   K  
   u     �     �     �     �     �  #   �                    .     I     Z     m  +   |     �     �     �     �      �       	   ,  B   6     y     �     �     �     �  
   �     �  	   �     �     �          +     ?     O     ]  	   l     v     �     �     �     �     �     �     �     �       (   	     2  3   I     }     �     �     �     �     �     �     �     �  	   �     �     	       ;   ,  6   h  �   �     H     <          &   !                        8      =          O   ,      +       I   N      -                       ?   F   9   W   3   	           :   Z      P   R       K                     ;   .              >   5   )      Y      M                "   B   2   1   6       L   J   G   4   Q               V       D   A   @      S                             (   7              $       T   H   C          
   X      E   %          *   /       #             0      U          '    *** You have to select only 1 ( or none ) portfolio category when enable this option. This option cannot works with carousel function. A Custom Post Type Plugin To Use With Goodlayers Theme ( This plugin functionality might not working properly on another theme ) About Post Author Add New Add New Portfolio All All Portfolios Ascending Order Carousel ( Only For Grid And Modern Style ) Category Classic No Space Style Classic Style Client Clients Descending Order Edit Portfolio Enable Pagination Enable Portfolio filter Feature Image FitRows ( Order items by row ) Goodlayers Goodlayers Portflio Post Type Goodlayers Portfolio Option Header Background Image Image Image Url Inside Portfolio Thumbnail Type Left Sidebar Lightbox Image Lightbox Video Lightbox to Full Image Link to Portfolio Link to URL Margin Bottom Masonry ( Order items by spaces ) Modern No Space Style Modern Style New Portfolio No portfolios found No portfolios found in Trash Num Excerpt Num Fetch Only effects to <strong>standard and gallery post format</strong> Open In New Tab Order Order By Page Caption Page Layout Page Option Pages: Portfolio Portfolio Categories Portfolio Category Portfolio Layout Order Portfolio Size Portfolio Style Portfolio Tag Portfolio Tags Portfolios Project Description Project Info Publish Date Random Related Projects Right Sidebar Same As Thumbnail Type Search Portfolio Skills Slider Spaces after ending of this item Specify Url Specify the number of portfolios you want to pull out. Stack Images Tag Tags Thumbnail Link Thumbnail Size Thumbnail Type Title Upload Video Video Url View Portfolio View Project Website Will be ignored when the portfolio filter option is enabled. You can see an example of these two layout here You can use Ctrl/Command button to select multiple categories or remove the selected category. <br><br> Leave this field blank to select all categories. http://www.goodlayers.com Project-Id-Version: Goodlayers Portflio Post Type
POT-Creation-Date: 2016-05-05 02:29+0200
PO-Revision-Date: 2016-05-05 02:55+0200
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: ..
X-Poedit-WPHeader: gdlr-portfolio.php
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 *** Musisz wybrać tylko jeden (lub brak) Kategoria portfelem kiedy włączyć tę opcję. Ta opcja nie może pracować z funkcją karuzeli. A Custom post Rodzaj wtyczki do użycia z Goodlayers Theme (Funkcja ta wtyczka może nie działać prawidłowo w innym temacie) O autorze post Dodaj nowy Dodaj nowe portfol Wszystko Wszystkie portfele Rosnąco Karuzela (tylko dla siatki i nowoczesnego stylu) Kategoria Klasyczny Brak miejsca Style classic Style Klient Klienci Kolejność malejąca Edycja Portfolio Włącz Pagination Włącz filtr Portfolio funkcja obrazu FitRows (pozycje Sortuj według rzędzie) Goodlayers Rodzaj Goodlayers Portflio post Opcja Goodlayers Portfolio Tło nagłówka Obraz Adres URL obrazka Wewnątrz Rodzaj Portfolio miniatur Lewy pas Lightbox Obraz Lightbox wideo Lightbox się pełny obraz Link do portfela Link do adresu URL margines dolny Murarskie (pozycje Sortuj według spacjami) Nowoczesne Brak miejsca Style nowoczesny Styl nowego portfela Nie znaleziono portfele Nie znaleziono portfolio w koszu Num Fragment Num Fetch Tylko efekty <strong> standardowy i galerii po formacie </ strong> Otwórz w nowej karcie Zamówienie Sortuj według Strona napisów Układ strony Opcja Page Strony: PORTFOLIO Kategorie Portfolio portfolio Kategoria Portfolio Układ Zamówienie Wielkość portfela portfolio Style portfolio Tag Portfolio Tagi Portfolio Opis Projektu Informacje o projekcie Data wydania: Losowo Projekty związane Prawy pasek Samo jak typ miniatur Szukaj Portfolio Możliwości Suwak Przestrzenie po zakończeniu tej pozycji Określanie adresu URL Określ liczbę pakietów, które chcesz wysunąć. Obrazy stos Słowo kluczowe Tagi thumbnail link Wielkość miniaturki Rodzaj thumbnail Tytuł Wyślij plik Wideo wideo Url Zobacz Portfolio Zobacz projekt Witryna internetowa Będą ignorowane, gdy opcja jest włączona filtr portfel. Można zobaczyć przykład tych dwóch układzie tutaj Można użyć przycisku Ctrl / polecenia, aby zaznaczyć wiele kategorii lub usunąć wybraną kategorię. <br> pozostaw to pole puste, aby wybrać wszystkie kategorie. http://www.goodlayers.com 